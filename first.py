import itertools
from functools import lru_cache
import utils
import math

def prim(ribs):
    vertices = list(utils.get_vertices(ribs))
    visited = []
    inf = float('inf')
    weight = 0
    cheapest_cost = {vertice: inf for vertice in vertices}
    cheapest_edge = {vertice: None for vertice in vertices}
    cheapest_cost[vertices[0]] = 0
    while len(vertices):
        v = min(vertices, key=lambda v: cheapest_cost[v])
        vertices.remove(v)
        weight += cheapest_cost[v]
        visited.append(v)
        if cheapest_edge[v] is not None:
            visited.append(cheapest_edge[v])
        for successor, rib in successors(v, ribs):
            if successor in vertices and rib.weight < cheapest_cost[successor]:
                cheapest_cost[successor] = rib.weight
                cheapest_edge[successor] = v
    return weight

def cruscal(ribs):
    weight = 0
    vertices = utils.get_vertices(ribs)
    subsets = {frozenset([v]) for v in vertices}
    for rib in sorted(ribs, key=lambda rib: rib.weight):
        start_subset = find_subset(subsets, rib.start)
        end_subset = find_subset(subsets, rib.end)
        if not start_subset == end_subset:
            weight += rib.weight
            subsets = join_subsets(subsets, start_subset, end_subset)
    return weight


def find_subset(superset, el):
    for subset in superset:
        if el in subset:
            return subset

def join_subsets(superset, first_sub, second_sub):
    return (superset - {first_sub, second_sub}) | {first_sub | second_sub}


@lru_cache(maxsize=32)
def successors(vertice, ribs):
    """returns list of tuples(adjacent vertice, rib)"""
    res = []
    for rib in ribs:
        if rib.start == vertice:
            res.append((rib.end, rib))
        elif rib.end == vertice:
            res.append((rib.start, rib))
    return res

def solve(algorithm, filename):
    ribs, verticesCnt, ribsCnt = utils.parse(filename)
    result = algorithm(ribs)
    open('output.txt', 'w').write(str(result))
    return result

print solve(cruscal, './input.txt')
