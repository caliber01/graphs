from collections import namedtuple

Rib = namedtuple('Rib', 'start, end, weight')

def parse(filename):
    f = open(filename, 'r')
    contents  = f.read()
    rows = contents.split('\n')
    verticesCnt, ribsCnt = rows[0].split(' ')
    ribs = [];
    for i in range(1, int(ribsCnt) + 1):
        start, end, weight = rows[i].split(' ')
        ribs.append(Rib(start, end, int(weight)))
    return tuple(ribs), verticesCnt, ribsCnt

def get_vertices(graph):
    # getting list of vertices
    vertices = set()
    for rib in graph:
        vertices.update({rib.start, rib.end})
    return vertices
