import utils

def ford_bellman(ribs, source):
    vertices = utils.get_vertices(ribs)
    inf = float('inf')
    distances = {vertice: inf for vertice in vertices}
    predecessors = {vertice: None for vertice in vertices}
    distances[source] = 0

    for vertice in vertices:
        for rib in ribs:
            if distances[rib.start] + rib.weight < distances[rib.end]:
                distances[rib.end] = distances[rib.start] + rib.weight
                predecessors[rib.end] = rib.start
    
    for rib in ribs:
        if distances[rib.start] + rib.weight < distances[rib.end]:
            raise Exception('negative weight cycle')

    return distances, predecessors

